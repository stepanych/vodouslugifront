const defaultTheme = require('tailwindcss/defaultTheme')


module.exports = {
  mode: 'jit',
  purge: {
    content: [
      './src/html/*.html',
      './src/hbs/!*.hbs',
      './src/hbs/partials/!**!/!*.hbs',
        ],
    options: {
      safelist: ['table.wcTable', 'blockquote', 'border-red'],
    }
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: ['Proxima Nova Rg', 'sans-serif'],
        'exo2': ['"Exo 2"']
      },
      lineHeight: {
        '4dot5': '4.5rem',
        '2dot15': '2.15rem'
      },
      colors: {
        blue: '#04385B',
        bluelight: '#4788E8',
        gray: '#C4C4C4',
        darkgray: '#798392',
        lightgray: '#F3F3F3',
        graybg: '#0B2232',
        mediumgray: '#898989',
        gray2: '#F0F0F0',
        gray3: '#9C9C9C',
        gray4: '#EBEBEB',
        bluegrad: '#04385b54'

      },
      spacing: {
        '17': '4.25rem',
        '18': '4.5rem',
        '19': '4.75rem',
        '21': '5.25rem',
        '30': '7.5rem',
        '4375':'4.375rem',
        '35625':'3.5625rem'
      },
      width: {
        '84': '21rem',
        '42': '10.5rem',
        '62': '14.25rem',
        '74': '19rem',
        '90': '21.25rem',
        'wc13': '3.125rem'
      },
      height: {
        '13': '3.35rem',
        '84': '21rem',
        '89': '22.5rem',
        '42': '10.5rem',
        'wc13': '3.125rem',
        'wc325': '3.25rem',
        'wc101': '103vh',
        '158125': '15.8125rem',
      },
      inset: {
        '13': '3.25rem',
        '18': '4.5rem',
        'p27': '27%',
        'wc65': '6.5rem'
      },
      scale: {
        "-1":"-1"
      },
      fontSize: {
        'base': '1em',
        'wc13': '1.3rem',
        'wc13p': '13px',
        'wc15': '15px',
        'wc14p': '14px',
        'wc18p': '1.125rem',
        'wc10': '0.625rem',
        'wc22': '1.375rem',
        '2lg': '2.5rem',
        '125': '1.25rem',
        '1625': '1.625rem',
        '2625': '2.625rem'
      },
    },
    screens: {
      'sm': '360px',
      'bmd': '480px',
      'ip': {'min': '768px', 'max': '959px'},
      'md': '960px',
      'lg': '1024px',
      'xl': '1440px'
    },
  },
  variants: {
    extend: {
      animation: ['hover'],
      backgroundImage: ['hover'],
      borderRadius: ['hover'],
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/line-clamp'),
    require('tailwind-scrollbar-hide')
  ],
}
