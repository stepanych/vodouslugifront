import './style.css';
import './wc.css';

import 'tiny-slider/src/tiny-slider.scss';
import { tns } from "tiny-slider/src/tiny-slider";
import isMobile from 'ismobilejs';



let top_menu_item_hover_init = () => {
    document.querySelectorAll('.top-menu .menu ul li a').forEach(item=>{
        item.addEventListener('mouseenter', event => {
            let li = event.target.closest('li');
            li.classList.add('hovered');
        });
        item.addEventListener('mouseleave', event => {
            let li = event.target.closest('li');
            li.classList.remove('hovered');
        });
    });
}

let footer_menu_item_hover_init = () => {
    document.querySelectorAll('.footer-menu ul li a').forEach(item=>{
        item.addEventListener('mouseenter', event => {
            let li = event.target.closest('li');
            li.classList.add('hovered');
        });
        item.addEventListener('mouseleave', event => {
            let li = event.target.closest('li');
            li.classList.remove('hovered');
        });
    });
}

let cat_list_item_hover_init = () => {
    document.querySelectorAll('.cat-menu ul li a').forEach(item=>{
        item.addEventListener('mouseenter', event => {
            let li = event.target.closest('li');
            li.classList.add('hovered');
        });
        item.addEventListener('mouseleave', event => {
            let li = event.target.closest('li');
            li.classList.remove('hovered');
        });
    });
}


let menu_btn_mob_init = () => {
    let sidebar = document.querySelector('#sidebar');
    document.querySelector('.menu-btn-mob').addEventListener('click', event => {
        event.preventDefault();
        sidebar.classList.add('opened');
    });
    document.querySelector('.menu-btn-mob-close').addEventListener('click', event => {
        event.preventDefault();
        sidebar.classList.remove('opened');
    });

    let menu = document.querySelector('ul.left-menu');
    document.querySelector('.menu_mob_catalog_toggler').addEventListener('click', event => {
        event.preventDefault();
        event.target.classList.toggle('opened');
        menu.classList.toggle('opened');
    });


}

let form_placeholders_init = () => {
    let wc_forms = document.querySelectorAll('.wc-form');

    wc_forms.forEach(item => {
            item.addEventListener('focusout', event => {
                if (event.target.value.trim() !== "") {
                    event.target.classList.add('filled');
                } else {
                    event.target.classList.remove('filled');
                }
            });
        }
    );
}

let checkbox_replace = () => {
    document.querySelectorAll('.wc-check>span').forEach(item => {
            item.addEventListener('click', event => {
                if (event.target.tagName.toLowerCase() == 'a') {
                    return false;
                }
                let wc_check = event.target.parentElement;
                let check = wc_check.querySelector('[type=checkbox]');
                wc_check.classList.toggle('checked');
                check.checked = !check.checked;
            })
        }
    );
}

let popups_init = () => {
    if (!document.querySelectorAll('[data-popup]')) {
        return;
    }
    let body = document.body;
    document.querySelectorAll('[data-popup]').forEach( item => {
        item.addEventListener('click', event => {
            event.preventDefault();

            let link = event.target;
            let popup_target = link.dataset.popup;
            let popup = document.querySelector(`[data-form=${popup_target}]`);

            if (!popup){
                return false;
            }
            body.classList.add('popup-shown');
            popup.classList.add('show');
            setTimeout(function() {
                popup.querySelector('.popup-wrapper').classList.add('show');
            }, 1);
        })
    });

    if (!document.querySelector('.popup')) {
        return;
    }
    document.querySelectorAll('.popup').forEach( item => {
        item.addEventListener('click', event => {
            if (event.target === event.currentTarget
                || event.target.classList.contains('close-btn')
            ) {
                body.classList.remove('popup-shown');
                let target = event.currentTarget;
                target.querySelector('.popup-wrapper').classList.remove('show');
                setTimeout(function () {
                    target.classList.remove('show');
                }, 1);
            }
        }, false);
    });
}

function wrap(el, wrapper) {
    el.parentNode.insertBefore(wrapper, el);
    wrapper.appendChild(el);
}

let make_table_responsive = () => {
    document.querySelectorAll('.wcTable').forEach( item => {
        if (!item.parentElement.classList.contains('table_over')){
            let div = document.createElement('div');
            wrap(item, div);
            div.classList.add('table_over');
        }
    });
}

let make_uls = () => {
    document.querySelectorAll('.content ul li').forEach( item => {
        if (!item.querySelector('.wcli')){
            let span = document.createElement('span');
            span.innerHTML =  item.innerHTML;
            item.innerHTML = "";
            item.appendChild(span);
            span.classList.add('wcli');
        }
    });
}

let equal_hgeight = (param) => {
    let max_height = 0;
    let heights = [];
    document.querySelectorAll('.'+param).forEach( item => {
        heights.push(item.offsetHeight);
    });

    max_height =  Math.max(...heights);

    document.querySelectorAll('.'+param).forEach( item => {
        item.style.height = max_height + "px";
    });
}
top_menu_item_hover_init();
footer_menu_item_hover_init();
menu_btn_mob_init();
form_placeholders_init();
checkbox_replace();
popups_init();
make_table_responsive();
make_uls();
cat_list_item_hover_init();

equal_hgeight('advantages ul li span p');
equal_hgeight('advantages ul li span h5');

if (!isMobile.any) {
    equal_hgeight('cat_list');
}

let is_home_page = document.querySelector('.home');
let is_work_page = document.querySelector('.work');
let is_review_slider_exist = document.querySelector('.review-slider');

if ( document.querySelector('.my-slider') ){
    let slider_count = document.querySelector('.my-slider').dataset.count||1;
    let is_loop = document.querySelector('.my-slider').dataset.loop||false;
    let is_auto = document.querySelector('.my-slider').dataset.auto||false;
    let slider = tns({
        container: '.my-slider',
        items: 1,
        mouseDrag: true,
        controls: false,
        nav: true,
        navPosition: "bottom",
        navContainer: ".slider-nav",
        slideBy: 'page',
        autoplay: is_auto,
        autoplayButtonOutput: false,
        loop: is_loop,
        responsive: {
            900: {
                items: slider_count
            }
        }
    });
}
if (is_home_page) {
    let small_slider = tns({
        container: '.small-slider',
        items: 1,
        mouseDrag: true,
        controls: true,
        nav: true,
        navPosition: "bottom",
        navContainer: ".small-slider-nav",
        slideBy: 'page',
        autoplay: false,
        prevButton: ".small-slider-prev",
        nextButton: ".small-slider-next",
    });
}

if (is_work_page) {
    if (document.querySelector('.work-slider')) {
        let workslider = tns({
            container: '.work-slider',
            items: 1,
            mouseDrag: true,
            controls: true,
            nav: true,
            navPosition: "bottom",
            navContainer: ".work-slider-nav",
            slideBy: 'page',
            autoplay: false,
            prevButton: ".work-slider-prev",
            nextButton: ".work-slider-next",
            autoWidth: true
        });
    }
}

if (is_review_slider_exist) {
    let reviewslider = tns({
        container: '.review-slider',
        items: 1,
        mouseDrag: true,
        controls: true,
        nav: true,
        navPosition: "bottom",
        navContainer: ".review-slider-nav",
        slideBy: 'page',
        autoplay: false,
        prevButton: ".review-slider-prev",
        nextButton: ".review-slider-next",
        autoWidth: true
    });
}

if (window.diafan_ajax) {
    diafan_ajax.success["feedback_add"] = function(form, response){

        let message = "";
        for (let err in response.errors){
            let name = err;
            message = response.errors[err].replace(/<\/?[^>]+>/gi, '');
        }

        if (response.result === "success") {
            if (form[0].classList.contains('consult')) {
                let msg = '<div class="flex justify-center items-center w-full p-4">'+message+'</div>';
                form[0].querySelector('.inp_wrapper').innerHTML = msg;
            }
        }

    }
}


let search_toggler = document.querySelector('.search-btn');
if (search_toggler){
    search_toggler.addEventListener('click', event => {
        document.querySelector('.search-block').classList.toggle('hidden');
        search_toggler.classList.toggle('opened');
    });
}

const contentWrapper = document.getElementById('content-wrapper');
document.addEventListener("DOMContentLoaded", function(event) {
    var scrollpos = localStorage.getItem('scrollpos');
    setTimeout( function () {
        if (scrollpos) contentWrapper.scrollTo(0, scrollpos);
    }, 50);
});

window.onbeforeunload = function(e) {
    localStorage.setItem('scrollpos', contentWrapper.scrollTop);
};
