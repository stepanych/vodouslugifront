const path = require('path');
const webpack = require("webpack");
const CopyPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HandlebarsPlugin = require("handlebars-webpack-plugin");
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const { HotModuleReplacementPlugin } = require('webpack')

const mergeJSON = require('handlebars-webpack-plugin/utils/mergeJSON');
const projectData = mergeJSON(path.join(__dirname, "src/hbs/data/*.json"));

const MangleCssClassPlugin = require('mangle-css-class-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');

const BrowserSyncPlugin = require('browser-sync-webpack-plugin')


const cssObfuscatorPlugin = new MangleCssClassPlugin({
    classNameRegExp: '(xs:|bmd:|md:|sm:|lg:|xl:)?wc-[a-zA-Z0-9_-]*',
    ignorePrefix: ['xs:', 'bmd:', 'md:', 'sm:', 'lg:', 'xl:'],
    log: true,
});

const FontPreloadPlugin = require("webpack-font-preload-plugin");

module.exports = {
    //mode: 'production',
    entry: [
        './src/home.js',
        //'./src/contacts.js',
        //'./src/work.js',
    ],
    stats: {
      children: true,
    },
    plugins: [
        /*new BrowserSyncPlugin({
            // browse to http://localhost:3000/ during development,
            // ./public directory is being served
            host: 'localhost',
            port: 3000,
            server: { baseDir: ['dist'] }
        }),*/
        new FontPreloadPlugin(),
        new MiniCssExtractPlugin({
            filename: "styles.css",
            chunkFilename: "styles.css"
        }),
        /*new HtmlWebpackPlugin({
            title: "home",
            filename: 'home.html',
            template: "src/html/home.html",
            minify: false
        }),
        new HtmlWebpackPlugin({
            title: "contacts",
            filename: 'contacts.html',
            template: "src/html/contacts.html",
            minify: false
        }),
        new HtmlWebpackPlugin({
            title: "services",
            filename: 'services.html',
            template: "src/html/services.html",
            minify: false
        }),
        new HtmlWebpackPlugin({
            title: "works",
            filename: 'works.html',
            template: "src/html/works.html",
            minify: false
        }),
        new HtmlWebpackPlugin({
            title: "work",
            filename: 'work.html',
            template: "src/html/work.html",
            minify: false
        }),
        new HtmlWebpackPlugin({
            title: "workdetail",
            filename: 'workdetail.html',
            template: "src/html/workdetail.html",
            minify: false
        }),*/
        new HtmlWebpackPlugin({
            title: "sparta",
            filename: 'sparta.html',
            template: "src/html/sparta.html",
            minify: false,
            alwaysWriteToDisk: true
        }),
        new HtmlWebpackHarddiskPlugin(),
        //cssObfuscatorPlugin,
        new CopyPlugin({
            patterns: [
                { from: path.resolve(__dirname,"src")+"/images/", to: path.resolve(__dirname,"dist")+"/custom/wc/images", force: true },
                { from: path.resolve(__dirname,"src")+"/fonts/", to: path.resolve(__dirname,"dist")+"/custom/wc/fonts", force: true },
            ],
        }),
        new HandlebarsPlugin({
            entry: path.join(process.cwd(), "src", "hbs", "sparta.hbs"),
            output: path.join(process.cwd(), "src", "html", "[name].html"),
            data: projectData,
            partials: [
                path.join(process.cwd(), "src", "hbs", "partials", "*", "*.hbs"),
                path.join(process.cwd(), "src", "hbs", "partials", "blocks", "*", "*.hbs"),
            ],
            helpers: {
                nameOfHbsHelper: Function.prototype,
                projectHelpers: path.join(process.cwd(), "src", "hbs", "helpers", "*.helper.js")
            },
            onBeforeSetup: function (Handlebars) {},
            onBeforeAddPartials: function (Handlebars, partialsMap) {},
            onBeforeCompile: function (Handlebars, templateContent) {},
            onBeforeRender: function (Handlebars, data, filename) {},
            onBeforeSave: function (Handlebars, resultHtml, filename) {},
            onDone: function (Handlebars, filename) {}
        }),
        new CleanWebpackPlugin(),


    ],
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
    },
    module: {
        rules: [
            {
                test: /\.js$/i,
                include: path.resolve(__dirname, 'src'),
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                    },
                },
            },
            {
                test: /\.css$/i,
                include: path.resolve(__dirname, 'src'),
                use: [
                    MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader'
                ],
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    // Translates CSS into CommonJS
                    "css-loader",
                    // Compiles Sass to CSS
                    "sass-loader",
                ],
            },
            {
                test: /\.(png|jpe?g|gif|svg)$/i,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: '/custom/wc/images/'
                        }
                    },
                ],
            },
            {
                test: /\.(woff(2)?|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: '/custom/wc/fonts/'
                        }
                    }
                ]
            }
        ],
    },
    devServer: {
        hot: 'only',
        static:  path.resolve(__dirname, 'dist'),
    },
    optimization: {
        minimize: true,
        minimizer: [
            // For webpack@5 you can use the `...` syntax to extend existing minimizers (i.e. `terser-webpack-plugin`), uncomment the next line
            `...`,
            new CssMinimizerPlugin(),
        ],
    },
};
